use std::collections::HashMap;
use std::io;
use std::path::Path;
use std::io::Write;

pub mod lib;

// main entry point
fn main() -> io::Result<()> {
    // Fetching number of IDs to be created and stored in a file from user
    println!("Please input number of IDs to create:");
    let len = lib::read_num();
    // fetching max number of repeats allowed from user
    println!("Please input maximum allowed occurrences per ID:");
    let max_repeats = lib::read_num();
    // Creating IDs and storing in a Vec<String> as well as a hashmap (for occurrences)
    let (h_map,vector) = lib::create_ids(len, max_repeats);

    // Setting file path, to be used by fs::write
    let file_path = Path::new("./data.txt");

    // Writes from vector to (a new) file with linebreak as delimeter between each string/element
    // Overwrites existing file or creates a new one if it doesn't exist
    std::fs::write(file_path, vector.join("\n"))?;

    // Checking for and printing number of repeated IDs in the file/vector
    let repeats = lib::check_occurrences(h_map);
    println!("max repeats: {:?}", repeats.unwrap_or(String::from("No IDs found")));

    Ok(())
}

/*
Random ID Gen Task (outline):
-write a random gen creating unique IDs (alphanumeric) of a specified length
-stores IDs in file
-user specified amount of entries in file (e.g., a million)
-user specified amount of max repeated entries (e.g., max a thousand repeats of an ID)
-implement a basic test suite
 */

use std::collections::HashMap;
use std::hash::Hash;
use std::io;
use std::io::Write;
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

// create_ids - Function that takes two inputs (from user); len - number of IDs to create, and repeats - max allowed repeats of
// each ID created. Returns a vector of all the IDs created and a hashmap with ID strings mapped to their occurrences
pub fn create_ids(len: i32, repeats: i32) -> (HashMap<String,usize>, Vec<String>){
    let mut num_ids = len;
    // Number of chars per random string id
    let id_length = 10; // <-- could make this one user-determined as well, for even more customization
    // Initializing m and id_vec
    let mut m: HashMap<String, usize> = HashMap::new();
    let mut id_vec = Vec::with_capacity(1000000);

    // Range of IDs to create
    for _iter in 0..num_ids {
        // Creating a random alphanumerical ID of length 'id_length'
        let rand_string: String = thread_rng()  
            .sample_iter(&Alphanumeric)
            .take(id_length)
            .map(char::from)
            .collect();

        // If the string is already in the hashmap:
        if m.contains_key(&rand_string.clone()) {
            // If the repeats are below the accepted amount 'repeats'
            if m[&rand_string.clone()] < repeats as usize {
                // Updates hashmap with new occurrence number, and pushes ID to vec
                *m.entry(rand_string.clone()).or_default() += 1;
                id_vec.push(rand_string.clone());
            } else {
                // If it's a repeat above threshold, increase amount of IDs created by one instead of using the ID created
                num_ids = num_ids + 1
            }
        } else {    // Key doesn't exist in hashmap, adding it to both to hashmap and vector
            m.insert(rand_string.clone(),1);
            id_vec.push(rand_string.clone());
        }
    }
    // Returning m and id_vec
    // NB! (if all numbers repeat at the max number of allowed repeats before len is reached, vec is returned with less than len IDs!)
    (m, id_vec)
}

// read_num - Function that reads a number from stdin and returns it as i32
pub fn read_num() -> i32{
    // Fetching number (actually, just a line) from stdin (user)
    let mut input_line = Default::default();
    io::stdin() //
        .read_line(&mut input_line) // actually read the line
        .expect("Failed to read line");
    // Parsing input to a i32 (if possible) or throws an error
    let x: i32 = input_line
        .trim() // ignore whitespace around input
        .parse() // convert to integers
        .expect("Input not an integer"); // In case of invalid input
    x
}


// Returns the repeated string and the number of repeats (if found) or None if not
pub fn check_occurrences(h_map: HashMap<String, usize>) -> Option<String>{
    // Finds the ID with the highest number of reoccurrences, and saves the key and value as an Option enum (some if found, otherwise none)
    let max = h_map.into_iter().max_by_key(|(_, v)| *v).map(|(k, v)| k + " : " + &*v.to_string());

    return max
}


// Basic test suite
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        // ID strings used to test check_occurrences()
        let st : String = String::from("jsd72b");
        let st2 : String = String::from("dwadsf");
        // Pushing strings to vector
        let v : Vec<String> = vec![st.clone(), st2.clone(), st.clone()];
        // Creating hashmap and inserting strings and their occurrences
        let mut hash_m: HashMap<String, usize> = HashMap::new();
        hash_m.insert(st.clone(),2);
        hash_m.insert(st2.clone(),1);
        // Storing the output from check_occurrences in 'result' variable
        let result = check_occurrences(hash_m);

        // Creating the string for the expected result
        let num = String::from(" : 2");
        // Making sure they're equal
        assert_eq!(result, (Some(st.clone() + &*num)));


    }
}



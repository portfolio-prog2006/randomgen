# Random Generator 

This project gave experience in using `rand` and HashMaps in Rust

## Name
Random Generator of Alphanumerical IDs

## Project brief
This project takes inputs from the user in standard input CLI, and creates random strings of alphanumerical chars, and then prints these to a file. 
The user controls number of IDs to create, as well as how many repeats of any IDs is allowed maximum. The code is easily modulated, and functionality for letting user decide length of ID strings can be implemented. 

## Task Outline (The Goals of the Program)
- [ ] write a random gen creating unique IDs (alphanumeric) of a specified length
- [ ] store these IDs in file
- [ ] there should be a user specified amount of entries/IDs in file (e.g., 10000)
- [ ] there should be a user specified amount of max allowed repeated entries (e.g., max 100 repeats of each ID)
- [ ] implement a basic test suite

The task was completed successfully, and the program can in just a few seconds create and write e.g. a million IDs to file. 


## Installation / Running the project
```
clone repo

cargo build
cargo run
```

